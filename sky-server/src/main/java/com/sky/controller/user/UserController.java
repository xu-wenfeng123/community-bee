package com.sky.controller.user;

import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;
import com.sky.properties.JwtProperties;
import com.sky.result.Result;
import com.sky.service.UserService;
import com.sky.utils.JwtUtil;
import com.sky.vo.UserLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/user/wxUser")
@Api(tags = "微信用户端相关接口")
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private JwtProperties jwtProperties;

    @PostMapping("/login")
    @ApiOperation("微信登录接口")
    public Result<UserLoginVO> login(@RequestBody UserLoginDTO userLoginDTO) {

        //1.微信登录
        User user = userService.wxLogin(userLoginDTO);
        //2.为微信用户生成令牌
        //2.1创建一个map对象，存入查询到user用的ID
        HashMap<String, Object> claims = new HashMap<>();
        claims.put("userId", user.getId());
        //2.2利用JwtUtil.createJWT（）方法生成微信登录后的token
        String token = JwtUtil.createJWT(jwtProperties.getUserSecretKey(), jwtProperties.getUserTtl(), claims);
        //3.封装到userLoginVO类作为登录成功的返回
        UserLoginVO userLoginVO = UserLoginVO.builder().openid(userLoginDTO.getCode()).id(user.getId()).token(token).build();
        return Result.success(userLoginVO );
    }
}
