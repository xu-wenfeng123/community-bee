package com.sky.annotation;

import com.sky.enumeration.OperationType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义注解，用于标识某个方法需要进行功能字段自动填充处理
 */
//规定注解只能加载方法上
@Target(ElementType.METHOD)
//@Retention(RetentionPolicy.RUNTIME)：固定写法，是 Java 注解（Annotation）的一个元注解（meta-annotation），它用于指定注解的生命周期。Java 注解本身有四种保留策略，它们分别由 java.lang.annotation.RetentionPolicy 枚举类型的四个值来表示
@Retention(RetentionPolicy.RUNTIME)
public @interface AutoFill {
    //指定数据库操作类型：update和insert
    OperationType value();
}
